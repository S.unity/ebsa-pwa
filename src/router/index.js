import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Quest from '@/components/Quest'
import Osago from '@/components/Osago'
import Docs from '@/components/Docs'
import Webinars from '@/components/Webinars'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello,
      meta: 'Главная'
    },
    {
      path: '/quest/:id',
      name: 'Quest',
      component: Quest,
      props: true,
      meta: null
    },
    {
      path: '/osago',
      name: 'Osago',
      component: Osago,
      meta: 'ОСАГО'
    },
    {
      path: '/docs',
      name: 'Docs',
      component: Docs,
      meta: 'Документы'
    },
    {
      path: '/webinars',
      name: 'Webinars',
      component: Webinars,
      meta: 'Онлайн-программы'
    },
    {
      path: '/profile',
      name: 'Profile',
      component: () => import('@/components/Profile'),
      meta: 'Мой профиль'
    },
    {
      path: '/my-chance',
      name: 'My_chance',
      component: () => import('@/components/My_chance'),
      meta: 'Мой шансы'
    },
    {
      path: '/subscription',
      name: 'Subscription',
      component: () => import('@/components/Subscription'),
      meta: 'Подписка'
    }
  ],
  // mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
